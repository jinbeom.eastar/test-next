'use client';

import { useRouter } from 'next/navigation';
import { useEffect } from 'react';

export default function useNetFUNNEL() {
    const router = useRouter();
    useEffect(() => {
        setTimeout(() => {
            console.log('NF START!!!');
            window.NFStart(
                {
                    projectKey: 'service_110',
                    segmentKey: 'segKey_5455'
                },
                function (response: any) {
                    router.push('/step1');
                    console.log(response);
                }
            );
        }, 1000);
    }, [router]);
}

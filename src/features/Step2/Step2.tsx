'use client';

import Link from 'next/link';
import useSelectStore from '../Step1/stores/select';
import { useUserStore } from '@/stores/user/provider';
import Typhography from '@/eastar-common/design-system/Typography';
import { eastar000001 } from '@/eastar-common/resources/lang';
import { useState } from 'react';

import revalidate from './initCache';
import { getData2, getData3 } from '@/apis/api';

export default function Step2() {
    const lang = useUserStore((state) => state.lang);
    const name = useUserStore((state) => state.name);
    const type = useSelectStore((state) => state.type);
    const [data, setData] = useState('');
    const [data2, setData2] = useState('');

    const onClick = async () => {
        setData('fetching...');
        setData2('fetching...');

        // getData2().then((d) => {
        //     setData(d?.unixtime ?? '');
        // });

        // getData3().then((d2) => {
        //     setData2(d2?.unixtime ?? '');
        // });

        fetch('/api/cached')
            .then((d) => d.json())
            .then((d) => {
                setData(d?.unixtime ?? '');
            });

        getData3().then((d2) => {
            setData2(d2?.unixtime ?? '');
        });
        revalidate('cachedTime');
    };
    return (
        <div className="container">
            <Typhography className="mb-[20px] py-[20px]" variant="h1">
                {eastar000001[lang]}
            </Typhography>
            <div className="flex h-64 items-center justify-center">{type} 항공편 선택 Component</div>
            <div>
                <button onClick={onClick}>1st Data : {data}</button>
            </div>
            <div>2nd Data : {data2}</div>
            <div>
                <button onClick={() => revalidate('cachedTime')}>1st 캐시 초기화</button>
            </div>
            <div>빌드 시점 데이터 : {name}</div>
            <div className="mt-10 flex justify-center gap-x-4 font-bold">
                <Link className="rounded border px-10 py-4 text-typography" href={'/step1'}>
                    이전
                </Link>
                <Link className="rounded border bg-primary px-10 py-4 text-white" href={'/step1'}>
                    다음
                </Link>
            </div>
        </div>
    );
}

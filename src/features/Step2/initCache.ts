'use server';

import { revalidatePath, revalidateTag } from 'next/cache';

export default async function revalidate(tag: string) {
    console.log(tag);
    revalidateTag(tag);
    revalidatePath('/api/cached');
    revalidatePath('/step2', 'page');
}

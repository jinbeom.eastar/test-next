import { Suspense } from 'react';
import ClientComp from './ClientComp';
import ServerComp from './ServerComp';
import { getData1 } from '@/apis/api';

export default async function CacheTestStatic() {
    const d = await getData1();

    await new Promise((r) => setTimeout(r, 1000));
    return (
        <div>
            <ServerComp />
            <ClientComp />
            {/* {Date.now()} */}
        </div>
    );
}

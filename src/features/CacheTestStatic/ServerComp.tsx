import { getData1 } from '@/apis/api';
import { cookies } from 'next/headers';
export default async function ServerComp() {
    const d = await getData1();
    // const c = cookies();
    // console.log(d, c);

    return <div>Static Server Build Time: {d.unixtime}</div>;
}

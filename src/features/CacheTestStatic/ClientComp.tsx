'use client';

import { getData1 } from '@/apis/api';
import { useEffect, useState } from 'react';

export default function ClientComp() {
    const [now, setNow] = useState<number>();

    useEffect(() => {
        getData1().then((d) => {
            setNow(d.unixtime);
        });
    }, []);

    return (
        <>
            <div>Client Build Time: {now}</div>
        </>
    );
}

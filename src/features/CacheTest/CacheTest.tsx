import ClientComp from './ClientComp';
import ServerComp from './ServerComp';

export default function CacheTest() {
    return (
        <div>
            <ServerComp />
            <ClientComp />
        </div>
    );
}

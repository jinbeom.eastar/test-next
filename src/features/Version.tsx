'use client';
import pkg from '../../package.json';

export default function useVersion() {
    console.log(`v${pkg.version}`);
    return null;
}

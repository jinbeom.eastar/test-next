import { cn } from '@/lib/utils';
import useSelectStore from '../stores/select';

export default function Select() {
    const { change, type } = useSelectStore((state) => ({ change: state.change, type: state.type }));

    return (
        <div className="flex gap-x-4">
            <button className={cn('rounded border px-2 py-1 shadow box-border', type === 'round' && 'border-2 border-primary')} onClick={() => change('round')}>
                왕복
            </button>
            <button className={cn('rounded border px-2 py-1 shadow box-border', type === 'oneWay' && 'border-2 border-primary')} onClick={() => change('oneWay')}>
                편도
            </button>
            <button className={cn('rounded border px-2 py-1 shadow box-border', type === 'multi' && 'border-2 border-primary')} onClick={() => change('multi')}>
                다구간
            </button>
        </div>
    );
}

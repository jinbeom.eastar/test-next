import useSelectStore from '../stores/select';
import { useRouter } from 'next/navigation';

export default function Booking() {
    const type = useSelectStore((state) => state.type);
    const routr = useRouter();
    const onClick = (e: React.MouseEvent) => {
        e.preventDefault();
        window.NFStart(
            {
                projectKey: 'service_110',
                segmentKey: 'segKey_5455'
            },
            () => {
                // window.NFStop({
                //     projectKey: 'service_110',
                //     segmentKey: 'segKey_5455'
                // });
                routr.push('/step2');
            }
        );
    };

    return (
        <>
            <div className="mt-10">
                {type === 'round' && <div>왕복</div>}
                {type === 'oneWay' && <div>편도</div>}
                {type === 'multi' && <div>다구간</div>}
            </div>
            <div className="mt-10 flex justify-center">
                <button className="rounded bg-primary px-12 py-5 text-lg font-bold text-white" onClick={onClick}>
                    항공편 조회
                </button>
            </div>
        </>
    );
}

import { create } from 'zustand';

type State = {
    type: 'round' | 'oneWay' | 'multi';
};

type Actions = {
    change: (type: State['type']) => void;
};

const useSelectStore = create<State & Actions>((set) => ({
    type: 'round',
    change: (type) => set({ type })
}));

export default useSelectStore;

'use client';

import Typhography from '@/eastar-common/design-system/Typography';
import lpk from '@/eastar-common/resources/lang';
import { useUserStore } from '@/stores/user/provider';
import Booking from './Booking/Booking';
import Select from './Select/Select';
import { useEffect } from 'react';

export default function Step1() {
    const lang = useUserStore((state) => state.lang);

    return (
        <div className="container">
            <Typhography className="mb-[20px] py-[20px]" variant="h1">
                {lpk.ReserveAirlineTicket[lang]}
            </Typhography>
            <Select />
            <Booking />
        </div>
    );
}

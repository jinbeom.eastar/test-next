import axios from 'axios';

const eastar = axios.create({ baseURL: 'https://localhost:9000', withCredentials: true });

export const getToken = async (cookie: string) => {
    const data = await eastar.get('/', { headers: { Cookie: cookie } }).then((r) => r.data);

    return data;
};

export const getExpiredToken = async () => {
    let data;
    try {
        data = await eastar.get('/err').then((r) => r.data);
    } catch (error) {
        console.log(error);
    }

    return data;
};

export const getData1 = async () => await fetch('https://worldtimeapi.org/api/timezone/Asia/Seoul?dummy=1').then((r) => r.json());
export const getData2 = async () => await fetch('https://worldtimeapi.org/api/timezone/Asia/Seoul?dummy=2').then((r) => r.json());
export const getData3 = async () => await fetch('https://worldtimeapi.org/api/timezone/Asia/Seoul?dummy=3').then((r) => r.json());

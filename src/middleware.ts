import { sources } from 'next/dist/compiled/webpack/webpack';
import { NextResponse } from 'next/server';
import type { NextRequest } from 'next/server';

/**
 * [참조] https://nextjs.org/docs/app/building-your-application/routing/middleware#nextresponse
 */

export async function middleware(request: NextRequest) {
    const nonce = Buffer.from(crypto.randomUUID()).toString('base64');
    // Replace newline characters and spaces

    console.log(request.url.match(/\.js\.map$/g), request.url, 'FADSFASDA');

    const cspHeader = `
        default-src 'self';
        script-src 'self' 'nonce-${nonce}' 'unsafe-inline' 'strict-dynamic' https://d-collect.jennifersoft.com/;
        style-src 'self' 'unsafe-inline';
        img-src 'self' blob: data:;
        font-src 'self' https://zeimage.eastarjet.com/;
        worker-src 'self' blob: data:;
        connect-src *;
        object-src 'none';
        base-uri 'self';
        form-action 'self';
        frame-ancestors 'none';
        upgrade-insecure-requests;
    `.replace(/\n/g, '');
    // Replace newline characters and spaces
    const contentSecurityPolicyHeaderValue = cspHeader.replace(/\s{2,}/g, ' ').trim();

    const requestHeaders = new Headers(request.headers);
    requestHeaders.set('x-nonce', nonce);

    requestHeaders.set('Content-Security-Policy', contentSecurityPolicyHeaderValue);

    const response = NextResponse.next({
        request: {
            headers: requestHeaders
        }
    });
    if (process.env.NODE_ENV === 'production') response.headers.set('Content-Security-Policy', contentSecurityPolicyHeaderValue);

    return response;
}

// See "Matching Paths" below to learn more
export const config = {
    matcher: [
        /*
         * Match all request paths except for the ones starting with:
         * - api (API routes)
         * - _next/static (static files)
         * - _next/image (image optimization files)
         * - favicon.ico (favicon file)
         */
        {
            source: '/((?!api|_next/static|_next/image|favicon.ico).*)',
            missing: [
                { type: 'header', key: 'next-router-prefetch' },
                { type: 'header', key: 'purpose', value: 'prefetch' }
            ]
        }
    ]
};

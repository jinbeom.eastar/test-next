import type { MetadataRoute } from 'next';

export default function sitemap(): MetadataRoute.Sitemap {
    return [
        {
            url: 'https://beom.vercel.app/',
            lastModified: new Date(),
            changeFrequency: 'monthly',
            priority: 1
        },
        {
            url: 'https://beom.vercel.app/step1',
            lastModified: new Date(),
            changeFrequency: 'monthly',
            priority: 0.8
        },
        {
            url: 'https://beom.vercel.app/step2',
            lastModified: new Date(),
            changeFrequency: 'monthly',
            priority: 0.5 // default
        }
    ];
}

import CacheTestStatic from '../../features/CacheTestStatic/CacheTestStatic';
import ClientComp from '@/features/CacheTestStatic/ClientComp';
import { cookies } from 'next/headers';
import { Suspense } from 'react';

export default function page() {
    // const c = cookies();
    // console.log(c);
    return (
        <>
            <CacheTestStatic />
            <Suspense fallback={'로딩중!!!!'}>
                <ClientComp />
            </Suspense>
        </>
    );
}

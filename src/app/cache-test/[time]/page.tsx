import CacheTest from '@/features/CacheTest/CacheTest';

export function generateStaticParams() {
    return [{ time: '1' }];
}

export default function page({ params }: { params: { time: string } }) {
    return <CacheTest />;
}

import { getData3 } from '@/apis/api';

export async function GET() {
    const d = await getData3();
    return Response.json(d);
}

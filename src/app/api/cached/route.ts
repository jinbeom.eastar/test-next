import { getData2 } from '@/apis/api';

export async function GET() {
    const d = await getData2();
    return Response.json(d);
}

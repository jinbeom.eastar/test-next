'use client';

import { PropsWithChildren, createContext, useContext, useRef } from 'react';
import { UserState, UserStore, createUserStore } from '.';
import { StoreApi, useStore } from 'zustand';

/**
 * create context
 */
export const UserStoreContext = createContext<StoreApi<UserStore> | null>(null);

/**
 * create store provider
 */
export const UserStoreProvider = ({ children, data }: PropsWithChildren<{ data: UserState }>) => {
  const storeRef = useRef<StoreApi<UserStore>>();
  if (!storeRef.current) {
    storeRef.current = createUserStore(data);
  }

  return <UserStoreContext.Provider value={storeRef.current}>{children}</UserStoreContext.Provider>;
};

/**
 * create use-store hook
 */
export const useUserStore = <T,>(selector: (store: UserStore) => T): T => {
  const user = useContext(UserStoreContext);

  if (!user) {
    throw new Error(`Can not find User Store Context`);
  }

  return useStore(user, selector);
};

import { createStore } from 'zustand/vanilla';

export type UserState = {
    id: string;
    name: string;
    login?: boolean;
    lang: 'kr' | 'us' | 'cn' | 'tw' | 'jp' | 'ru' | 'th';
};

export type UserAction = {
    setLang: (lang: UserState['lang']) => void;
};

export type UserStore = UserState & UserAction;

/**
 * init default store state
 */
export const defaultInitState: UserState = {
    id: '',
    name: '',
    lang: 'kr'
};

/**
 * create store
 */
export const createUserStore = (initState: UserState = defaultInitState) => {
    return createStore<UserStore>()((set) => ({
        ...initState,
        setLogin: () => set({ login: false }),
        setLang: (lang) => set({ lang })
    }));
};

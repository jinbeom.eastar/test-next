namespace NodeJS {
    interface ProcessEnv {
        NEXT_PUBLIC_FRONT_ENV: string;
        localEnv: string;
    }
}

declare global {
    interface Window {
        NFStart({ projectKey, segmentKey }: { projectKey: string; segmentKey: string }, callback: (response: any) => void): void;
        NFStop({ projectKey, segmentKey }: { projectKey: string; segmentKey: string }, callback?: (response: any) => void): void;
        NFStartSection({ projectKey, segmentKey }: { projectKey: string; segmentKey: string }, callback: (response: any) => void): void;
        NFStopSection({ projectKey, segmentKey }: { projectKey: string; segmentKey: string }, callback?: (response: any) => void): void;
    }
}

export {};

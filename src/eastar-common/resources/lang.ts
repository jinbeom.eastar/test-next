const lpk = {
    login: {
        kr: '로그인',
        us: 'Login',
        cn: '登录',
        tw: '登入',
        jp: 'ログイン',
        ru: 'Войти',
        th: 'เข้า สู่ ระบบ'
    },
    joinUs: {
        kr: '회원가입',
        us: 'Join Us',
        cn: '注册会员',
        tw: '加入會員',
        jp: '会員登録',
        ru: 'Регистрация участника',
        th: 'เข้าร่วมกับเรา'
    },
    onlineCheckIn: {
        kr: '온라인 체크인',
        us: 'Online Check-in',
        cn: '网上值机',
        tw: '網路報到',
        jp: 'オンラインチェックイン',
        ru: 'Веб-регистрация на посадку',
        th: 'เช็คอินออนไลน์'
    },
    customerCenter: {
        kr: '고객센터',
        us: 'Customer Center',
        cn: '客服中心',
        tw: '客服中心',
        jp: 'お客様センター',
        ru: 'Центр обслуживания клиентов',
        th: 'ศูนย์บริการลูกค้า'
    },
    ReserveAirlineTicket: {
        kr: '항공권 예매',
        us: 'Reserve airline ticket',
        cn: '机票预订',
        tw: '機票訂位',
        jp: '航空券のご予約',
        ru: 'Бронирование авиабилетов',
        th: 'จองตั๋วเครื่องบิน'
    },
    eventZone: {
        kr: '이벤트 존',
        us: 'Event Zone',
        cn: '易斯达群',
        tw: '活動專區',
        jp: 'イースターゾーン',
        ru: '이벤트 존',
        th: 'โซนกิจกรรม'
    },
    eastar000001: {
        kr: '항공편 선택',
        us: 'Select a Flight',
        cn: '选择航班',
        tw: '選擇航班',
        jp: 'フライト選択',
        ru: 'Выбор авиарейсов',
        th: 'เลือกเที่ยวบิน'
    }
};

/** 항공편 선택 */
export const eastar000001 = {
    kr: '항공편 선택',
    us: 'Select a Flight',
    cn: '选择航班',
    tw: '選擇航班',
    jp: 'フライト選択',
    ru: 'Выбор авиарейсов',
    th: 'เลือกเที่ยวบิน'
} as const;

/** 로그인 */
export const eastar000002 = {
    kr: '로그인',
    us: 'Login',
    cn: '登录',
    tw: '登入',
    jp: 'ログイン',
    ru: 'Войти',
    th: 'เข้า สู่ ระบบ'
} as const;

export default lpk;

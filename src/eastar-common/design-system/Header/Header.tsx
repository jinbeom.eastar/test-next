'use client';

import { useUserStore } from '@/stores/user/provider';
import lpk from '../../resources/lang';
import { useState } from 'react';
import { UserStore } from '@/stores/user';
import KR from './KR';
import RU from './RU';
import US from './US';

const list = { kr: '한국어', us: 'English', cn: '繁體中文', tw: '简体中文', jp: '日本語', ru: 'ru', th: 'th' };

const EventZone = () => (
    <>
        <li>|</li>
        <li>{lpk.eventZone['us']}</li>
    </>
);

export default function Header() {
    const [open, setOpen] = useState(false);
    const lang = useUserStore((state) => state.lang);
    const setLang = useUserStore((state) => state.setLang);
    const onLangListClick = () => setOpen((prev) => !prev);

    const onLangChange = (lang: UserStore['lang']) => {
        setLang(lang);
        setOpen(false);
    };

    return (
        <header className="bg-primary text-center text-[14px] font-medium -tracking-[0.3px] text-white">
            <div className="container h-[35px]">
                <ul className="flex h-full items-center justify-end gap-[10px] font-['Noto_kr']">
                    <li>{lpk.login[lang]}</li>
                    <li>|</li>
                    <li>{lpk.joinUs[lang]}</li>
                    <li>|</li>
                    <li>{lpk.onlineCheckIn[lang]}</li>
                    <li>|</li>
                    <li>{lpk.customerCenter[lang]}</li>
                    {lang === 'us' && <EventZone />}
                    <li className="relative w-24 text-center">
                        <button onClick={onLangListClick}>{list[lang]}</button>
                        {open && (
                            <div className="absolute left-0 right-0 bg-primary">
                                <ul className="py-2">
                                    <li className="cursor-pointer pb-2" onClick={() => onLangChange('kr')}>
                                        {list.kr}
                                    </li>
                                    <li className="cursor-pointer pb-2" onClick={() => onLangChange('cn')}>
                                        {list.cn}
                                    </li>
                                    <li className="cursor-pointer pb-2" onClick={() => onLangChange('jp')}>
                                        {list.jp}
                                    </li>
                                    <li className="cursor-pointer pb-2" onClick={() => onLangChange('us')}>
                                        {list.us}
                                    </li>
                                    <li className="cursor-pointer" onClick={() => onLangChange('tw')}>
                                        {list.tw}
                                    </li>
                                </ul>
                            </div>
                        )}
                    </li>
                </ul>
                <div className="text-black">
                    {/* {lang === 'kr' && <KR />}
                    {lang === 'ru' && <RU />}
                    {lang === 'us' && <US />} */}
                </div>
            </div>
        </header>
    );
}

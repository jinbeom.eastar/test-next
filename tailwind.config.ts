import type { Config } from 'tailwindcss';

const config = {
    darkMode: ['class'],
    content: ['./pages/**/*.{ts,tsx}', './components/**/*.{ts,tsx}', './app/**/*.{ts,tsx}', './src/**/*.{ts,tsx}'],
    prefix: '',
    theme: {
        extend: {
            container: {
                center: true,
                padding: {
                    DEFAULT: '1rem',
                    '2xl': '3rem'
                },
                screens: {
                    '2xl': '1400px'
                }
            },
            colors: {
                primary: {
                    DEFAULT: '#d11f26'
                },
                typography: {
                    DEFAULT: '#333333'
                }
            }
        }
    },
    extend: {
        keyframes: {
            'accordion-down': {
                from: { height: '0' },
                to: { height: 'var(--radix-accordion-content-height)' }
            },
            'accordion-up': {
                from: { height: 'var(--radix-accordion-content-height)' },
                to: { height: '0' }
            }
        },
        animation: {
            'accordion-down': 'accordion-down 0.2s ease-out',
            'accordion-up': 'accordion-up 0.2s ease-out'
        }
    },
    plugins: [require('tailwindcss-animate')]
} satisfies Config;

export default config;

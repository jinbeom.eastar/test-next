/** @type {import('next').NextConfig} */
const nextConfig = {
    productionBrowserSourceMaps: true,
    // nextjs cache 사용 체크
    logging: {
        fetches: {
            fullUrl: true
        }
    },
    webpack(config) {
        // Important: return the modified config

        return { ...config, devtool: 'hidden-source-map' };
    },
    // async redirects() {
    //     return [
    //         {
    //             source: '/',
    //             destination: '/step1',
    //             permanent: true
    //         }
    //     ];
    // }
};

// export default withSentryConfig(nextConfig, {
//     // For all available options, see:
//     // https://github.com/getsentry/sentry-webpack-plugin#options

//     org: 'eastarjet-w1',
//     project: 'javascript-nextjs',

//     // Only print logs for uploading source maps in CI
//     silent: !process.env.CI,

//     // For all available options, see:
//     // https://docs.sentry.io/platforms/javascript/guides/nextjs/manual-setup/

//     // Upload a larger set of source maps for prettier stack traces (increases build time)
//     widenClientFileUpload: true,

//     // Route browser requests to Sentry through a Next.js rewrite to circumvent ad-blockers.
//     // This can increase your server load as well as your hosting bill.
//     // Note: Check that the configured route will not match with your Next.js middleware, otherwise reporting of client-
//     // side errors will fail.
//     tunnelRoute: '/monitoring',

//     // Hides source maps from generated client bundles
//     hideSourceMaps: true,

//     // Automatically tree-shake Sentry logger statements to reduce bundle size
//     disableLogger: true,

//     // Enables automatic instrumentation of Vercel Cron Monitors. (Does not yet work with App Router route handlers.)
//     // See the following for more information:
//     // https://docs.sentry.io/product/crons/
//     // https://vercel.com/docs/cron-jobs
//     automaticVercelMonitors: true
// });

export default nextConfig;

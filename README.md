## Getting Started

[Git submodule 참조](https://git-scm.com/book/en/v2/Git-Tools-Submodules)

Add git submodule
```bash
# add submodule
git submodule add [repository] src/[name]

# update submodule
git submodule update --remote

# push submodule
git push --recurse-submodules=on-demand

# clone project with submodule
git clone --recurse-submodules [repository]
```

First, run the development server:
```bash
# install dependencies
npm ci

# run http development server
npm run dev

# run https development server
npm run httpsDev
```

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash

source ~/.bashrc

nvm install node
```